%global pypi_name portend

Name:           python-%{pypi_name}
Version:        2.3
Release:        1%{?dist}
Summary:        TCP port monitoring utilities
License:        MIT
URL:            https://github.com/jaraco/portend
Source0:        https://files.pythonhosted.org/packages/source/p/%{pypi_name}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  python3-devel
BuildRequires:  python3dist(pytest) >= 3.5
BuildRequires:  python3dist(setuptools)
BuildRequires:  python3dist(setuptools-scm) >= 1.15.0
BuildRequires:  python3dist(tempora) >= 1.8

%global _description\
Use portend to monitor TCP ports for bound or unbound states.

%description %_description

%package -n python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}
%description -n python3-%{pypi_name} %_description

%prep
%autosetup -n %{pypi_name}-%{version}

sed -i 's/--flake8//' pytest.ini
sed -i 's/--doctest-modules//' pytest.ini

%build
SETUPTOOLS_SCM_PRETEND_VERSION=%{version}
%{py3_build}

%install
%{py3_install}

%check
py.test-%{python3_version} -v

%files -n python3-%{pypi_name}
%license LICENSE
%doc README.rst
%{python3_sitelib}/__pycache__/*
%{python3_sitelib}/%{pypi_name}.py
%{python3_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info

%changelog
* Mon Feb 25 2019 Ken Dreyer <kdreyer@redhat.com> - 2.3-1
- Initial package.
